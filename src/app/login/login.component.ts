import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private authService: AuthService, private router: Router) {}

  urlApi = 'https://bem-ipb.herokuapp.com/user/';

  login = new FormGroup({
    email: new FormControl('', [Validators.email]),
    password: new FormControl('', [Validators.required]),
  });
  ngOnInit(): void {}
  onLogin = () => {
    const { email, password } = this.login.value;
    this.authService.login({ email, password }).subscribe((data: any) => {
      if (data) {
        this.router.navigate(['']);
        localStorage.setItem('access_token', data.token);
      }
    });
  };
  get email() {
    return this.login.get('email');
  }
}
