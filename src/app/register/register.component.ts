import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  register = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required),
    role: new FormControl('role', Validators.required),
  });
  urlApi = 'https://bem-ipb.herokuapp.com/user/';
  constructor(private http: HttpClient) {}
  onRegister() {
    const { name, role, email, password } = this.register.value;
    this.http
      .post(this.urlApi + 'register', { email, password, name, role })
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }
  get password() {
    return this.register.get('password')?.value;
  }
  get confirmPassword() {
    return this.register.get('confirmPassword')?.value;
  }
}
