import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionGuard } from './helpers/permission.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
RegisterComponent;

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [PermissionGuard] },
  { path: '', component: LoginComponent, canActivate: [PermissionGuard] },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [PermissionGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
