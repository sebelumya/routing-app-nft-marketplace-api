import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    //
  ],
})
export class HomeComponent implements OnInit {
  users: any = [];
  constructor(
    private http: HttpClient,
    private userService: UserService,
    private router: Router
  ) {}
  urlApi = 'https://6348c74aa59874146b10a6a5.mockapi.io/api/v1/users';
  ngOnInit(): void {
    this.getData();
  }
  getData() {
    const getData = this.userService.getUsers();
    getData.subscribe((data) => {
      this.users = data;
    });
  }
  logOut() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
