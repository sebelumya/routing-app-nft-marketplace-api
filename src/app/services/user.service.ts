import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  urlApi = 'https://6348c74aa59874146b10a6a5.mockapi.io/api/v1/users';
  users: any[] = [];
  constructor(private http: HttpClient) {}
  getUsers() {
    return this.http.get(this.urlApi);
    // .subscribe((data: any) => {
    //   this.users.push(data[0]);
    // });
    // return this.users;
  }
}
